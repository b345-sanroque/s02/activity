package com.zuitt.wdc044.models;

import javax.persistence.*;

//mark this Java Object as a representation of a database table via @Entity notation
@Entity

@Table(name = "posts")
public class Post {
    //indicate that this property represents the primary key;

    //1.PROPERTIES
    @Id
    //values for this property will be auto-incremented
    @GeneratedValue
    private long id;

    //class properties that represent table columns in a relational database and is annotated by Column
    @Column
    private String title;

    @Column
    private String content;

//    @ManyToOne
//    @JoinColumn(name = "user_id") //foreign key column
//    private User user;

    //2.CONSTRUCTORS
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    //3.GETTERS & SETTERS
        //no need for the properties, only Columns
    public String getTitle(){
        return this.title;
    }

    public String getContent(){
        return this.content;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }
}

//NOTES:
//Creating models

//in every models, there should be counterpart Interface repositories