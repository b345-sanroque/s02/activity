package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    //1.PROPERTIES
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String username;

    @Column
    private String password;

    //2.CONSTRUCTORS
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //3.GETTERS & SETTERS
    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    public void setUsername(String firstName){
        this.username = username;
    }

    public void setPassword(String lastName){
        this.password = password;
    }
}

//sorry po, lutang.. not reading instructions properly